from datetime import datetime
import json
from django.shortcuts import render
from Api.models import Orders
# V  DRF  V
from rest_framework.decorators import api_view
from rest_framework.response import Response

from Api.serializers import OrdersSerializer
from _Main.settings import CHUNK_SIZE, GSHEET_SSID, GSHEET_SHEET_ID, GSHEET_CONFIG_FILE


@api_view()
def get_chunk(request):
  chunkid = request.GET.get('chunkid', 0)
  chunkid = int(chunkid)
  print('requested chunk:', chunkid)
  entries = Orders.objects.order_by('table_row_index')[chunkid*CHUNK_SIZE:(chunkid + 1)*CHUNK_SIZE]
  return Response(OrdersSerializer(entries, many=True).data)

@api_view()
def get_ss_link(request):
  return Response({'link': f'https://docs.google.com/spreadsheets/d/{GSHEET_SSID}/#gid={GSHEET_SHEET_ID}'})

@api_view()
def get_last_update_time(request):
  last_updated = 0
  with open(GSHEET_CONFIG_FILE, 'r') as config_file:
    try:
      last_updated = json.load(config_file).get("lastUpdate")
    except Exception as e:
      print('GSHEET config error on reading config:' , e)
  if not last_updated:
    last_updated = datetime(0)
  return Response({'lastUpdateTime': last_updated})
