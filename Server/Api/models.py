# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Orders(models.Model):
  # id =    is created by default
  table_row_index = models.IntegerField(unique=True)
  table_row_number = models.IntegerField()
  order_number = models.IntegerField(unique=True)
  cost_usd = models.DecimalField(max_digits=12, decimal_places=2)
  cost_rub = models.DecimalField(max_digits=12, decimal_places=2)
  delivery_date = models.CharField(max_length=10)
  created_at = models.DateTimeField()
  updated_at = models.DateTimeField()

  class Meta:
    managed = False
    db_table = 'orders'
