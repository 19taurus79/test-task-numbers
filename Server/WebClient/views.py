from urllib import response
import logging
from django.http import HttpResponse, FileResponse
from django.shortcuts import render
from django.conf import settings

# https://www.youtube.com/watch?v=nAK-Tpc3NMI
logger = logging.getLogger(__name__)

def index_page_router(request):
  print('index_page_router')
  logger.info("index_page_router: " + request.path)
  return FileResponse(open(settings.STATIC_ROOT + '/index.html', 'rb'))
