// '2022-07-09T00:30:47.106284+03:00'

export function tranformTZDate(TZdate: string) {
  return (new Date(TZdate.replace(' ', 'T')).toLocaleDateString('ru-RU'));
}

export function tranformTZDateTime(TZdate: string) {
  const datetime = new Date(TZdate.replace(' ', 'T'));
  return datetime.toLocaleTimeString('ru-RU') + ' ' + datetime.toLocaleDateString('ru-RU');
}
