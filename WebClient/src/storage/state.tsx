import React, {Dispatch, FC, ReactNode, SetStateAction, useState} from 'react';

export type OrderT = {
  'table_row_number': number,
  'order_number': number,
  'cost_usd': string,
  'cost_rub': string,
  'delivery_date': string,
  'created_at': string,
};

const initialState = {
  spreadsheet: [] as OrderT[],
  lastUpdateCurrency: '',
  lastUpdateSpreadsheet: '',
  link: '',
  lastChunkId: 0,
};

export interface IStateContext {
  state: typeof initialState;
  setState: Dispatch<SetStateAction<typeof initialState>> | (() => void)
}

// eslint-disable-next-line @typescript-eslint/no-empty-function
export const StateContext = React.createContext<IStateContext>({state: initialState, setState: () => {}});

const StateProviderComponent: FC<{children?: ReactNode}> = ({children}) => {
  const [state, setState] = useState(initialState);
  return (
    <StateContext.Provider value={{state, setState}}>
      {children}
    </StateContext.Provider>
  );
};

export default StateProviderComponent;
