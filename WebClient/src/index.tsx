import React from 'react';
import ReactDOM from 'react-dom';

import MainPage from './pages/mainPage';
import StoreProvider from './storage/state';

ReactDOM.render(
  <React.StrictMode>
    <StoreProvider>
      <MainPage />
    </StoreProvider>
  </React.StrictMode>,
  document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals(console.log);
