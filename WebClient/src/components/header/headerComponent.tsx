import React, {FC} from 'react';

import './header.scss';

type HeaderMapNameT<N extends string = string> = N;

type HeaderNameLinkMapT<N extends string = string, L extends string = string> = {
  // eslint-disable-next-line no-unused-vars
  [key in HeaderMapNameT<N>]: {
    headerSign: string,
    link: L,
    target?: string,
  };
};

interface HeaderProps<N extends string = string, L extends string = string> {
  readonly nameLinkMapT: HeaderNameLinkMapT<N, L>,
  readonly displayedNames: N[],
  readonly nameOfActive: N;
}


/**
  * Универсальный компонент хедера.
  * Настраивается через конфиг.
  * @param {HeaderNameLinkMapT} headerLinksMap - карта соответствия: {
  *   headerSign: string;
  *   link: L;
  *   target?: string,
  * }
  * @param {string[]} displayedNames - массив тех ключей карты, которые будут отображаться
  * @param {string} nameOfActive - имя того ключа, который подствечивается как текущая страница
  * @return {JSX}
*/
const HeaderComponent: FC<HeaderProps> = ({nameLinkMapT, displayedNames, nameOfActive}) => (
  <nav>
    <div className="nav-wrapper theme-color">
      <a href="/" className="logo left">
        <i className="material-icons medium">home</i>
        Numbers - Кириллов
      </a>
      <ul id="nav-mobile" className="left hide-on-med-and-down">
        {displayedNames.map((page, i) => (
          <li className={page === nameOfActive ? 'active' : ''} key={i}>
            <a href={nameLinkMapT[page].link} target={nameLinkMapT[page].target}>{nameLinkMapT[page].headerSign}</a>
          </li>
        ))}
      </ul>
    </div>
  </nav>
);

export default HeaderComponent;
