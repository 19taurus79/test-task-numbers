import React, {FC} from 'react';
import {useTable} from 'react-table';
import type {OrderT} from '../../storage/state';

import {COLUMNS} from './columns';

import './table.scss';

// key = {i} is always in useTable() methods
/* eslint-disable react/jsx-key */

interface TablePropsI {
  data: OrderT[],
}

const TableComponent: FC<TablePropsI> = ({data}) => {
  // these methods come up with react-table
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    footerGroups,
    rows,
    prepareRow,
  } = useTable<OrderT>({
    columns: COLUMNS,
    data,
  });

  return (
    <table className='highlight' {...getTableProps()}>
      <thead>
        {headerGroups.map((headerGroup) => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map((column) => (
              <th {...column.getHeaderProps()}>{column.render('Header')}</th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()}>
        {rows.map((row) => {
          prepareRow(row);
          return (
            <tr {...row.getRowProps()}>
              {row.cells.map((cell) => (
                <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
              ))}
            </tr>
          );
        })}
      </tbody>
      <tfoot>
        {footerGroups.map((footerGroup) => (
          <tr {...footerGroup.getFooterGroupProps()}>
            {footerGroup.headers.map((column) => (
              <td {...column.getFooterProps()}>{column.render('Footer')}</td>
            ))}
          </tr>
        ))}
      </tfoot>
    </table>
  );
};

export default TableComponent;
