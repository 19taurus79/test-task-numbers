export const COLUMNS = [
  {
    Header: '№',
    Footer: '№',
    accessor: 'table_row_number',
    disableFilters: true,
    sticky: 'left',
  },
  {
    Header: 'Номер заказа',
    Footer: 'Номер заказа',
    accessor: 'order_number',
  },
  {
    Header: 'Стоимость, $',
    Footer: 'Стоимость, $',
    accessor: 'cost_usd',
    disableFilters: true,
    sticky: 'left',
  },
  {
    Header: 'Стоимость, Р',
    Footer: 'Стоимость, Р',
    accessor: 'cost_rub',
    disableFilters: true,
    sticky: 'left',
  },
  {
    Header: 'Добавлен',
    Footer: 'Добавлен',
    accessor: 'created_at',
  },
  {
    Header: 'Срок доставки',
    Footer: 'Срок доставки',
    accessor: 'delivery_date',
  },
] as const;

// 'table_row_index',
// 'table_row_number',
// 'order_number',
// 'cost_usd',
// 'cost_rub',
// 'delivery_date',
// 'created_at',
// 'updated_at',
