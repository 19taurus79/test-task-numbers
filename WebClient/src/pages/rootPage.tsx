import React, {FC} from 'react';
import HeaderComponent from '../components/header/headerComponent';

// //////////////////////////////////////////////////////
//
//            Стандартная страница приложения
//            Все вложенные элементы обернуты
//               в Materialize .container
//
// //////////////////////////////////////////////////////


interface PageProps {
  nameOfActive: string;
}

export const AppRoutingList = {
  'Main': {
    link: '/',
    headerSign: 'Главная',
    headerDefaultDisplay: true,
  },
  'About': {
    link: 'https://drive.google.com/drive/u/1/folders/1wGEuseZ5yueri8nXYpgnwzVcuZnZlKas',
    target: '_blank',
    headerSign: 'Об авторе',
    headerDefaultDisplay: true,
  },
} as const;

const RootPage: FC<PageProps> = ({nameOfActive, children}) => (
  <>
    <HeaderComponent
      nameLinkMapT={AppRoutingList}
      displayedNames={['Main', 'About']}
      nameOfActive={nameOfActive} />
    <div className="container">
      {children}
    </div>
  </>
);

export default RootPage;
