import React, {FC, useEffect} from 'react';
import RootPage from './rootPage';
import Table from '../components/table/tableComponent';

import '../index.scss';
import LoadMoreBtn from '../components/loadMoreBtn/loadMoreBtnComponent';
import Button from '../components/button/buttonComponent';
import useStore from '../hooks/useStore';
import {checkChanges, loadChunk, loadLink, reloadTable} from '../api/ajaxRequests';
import {useState} from 'react';

const CHANGES_POLL_INTERVAL = 10000; // 1 * 60 * 1000 - 1 min

const MainPage: FC = () => {
  const {state, setState} = useStore();
  // this flag guarantees that checkChanges will be called exectly after first loadChunk on
  // first page rander
  const [initialLoadingStep, setInitialLoadingStep] = useState(0);
  const [isChanged, setIsChanged] = useState(false);

  useEffect(() => {
    loadChunk(state, setState).then(() => setInitialLoadingStep(1));
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (initialLoadingStep === 1) {
      checkChanges(state, setState).then(() => setInitialLoadingStep(2));
    }
  }, [initialLoadingStep, state, setState, setInitialLoadingStep]);

  useEffect(() => {
    if (initialLoadingStep === 2) {
      loadLink(state, setState).then(() => setInitialLoadingStep(3));
    }
  }, [initialLoadingStep, state, setState, setInitialLoadingStep]);

  useEffect(() => {
    const int = setInterval(() => checkChanges(state, setState).then((time) => {
      if (time !== state.lastUpdateSpreadsheet) {
        console.warn('найдено обновление', state.lastUpdateSpreadsheet, '->', time);
        setIsChanged(true);
      }
    }), CHANGES_POLL_INTERVAL);
    return () => {
      clearInterval(int);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state, setState, setIsChanged]);


  return (
    <RootPage nameOfActive='Main'>
      <h3 className="light-blue-text text-lighten">Клон&nbsp;
        {state.link ? <a className="light-blue-text text-lighten text-underline" target='_blank'
          rel='noreferrer' href={state.link}>
          Google таблицы
        </a> : 'Google таблицы'}
      </h3>
      <div className='row'>
        <div className='col s4 flow-text'>
          БД обновлена:
          <br />
          {(state.lastUpdateSpreadsheet)}
        </div>
        <div className='col s5'>
          {isChanged ?
            <div className='warning red lighten-2 flow-text w-100 right'>
              <div className='jc-sp-btw '>
                <div className='d-flex truncate'>
                  Есть изменения!
                </div>
                <div className='d-flex'>
                  <i className="material-icons small">chevron_right</i>
                </div>
              </div>
            </div> : ''}
        </div>
        <div className='col s3'>
          <Button className='right' sign='Обновить' onClick={() => {
            console.log('обновление');
            setIsChanged(false);
            reloadTable(state, setState);
          }} />
        </div>
      </div>
      <div className='row'>
        <div className='col s11 flow-text'>
          Курс обновлен:&nbsp;{state.lastUpdateCurrency}
        </div>
      </div>
      <div className="row">
        <div className="plate col s12">
          <Table data={state.spreadsheet}/>
        </div>
      </div>
      <div className='row center-align'>
        <LoadMoreBtn />
      </div>
      <div className='row'>
      </div>
    </RootPage>
  );
};


export default MainPage;
