import {useContext} from 'react';
import {StateContext} from '../storage/state';

export default function useStore() {
  return useContext(StateContext);
}
