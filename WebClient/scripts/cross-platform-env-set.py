import sys
import os

def env_getter(var):
  # os.name = 'posix' | 'nt' | 'Java' ~  Unix / Windows / Java
  # or
  # sys.platform
  # https://docs.python.org/2/library/sys.html#sys.platform
  return '%' + var + '%' if sys.platform == 'win32' else '$' + var

def minimize_args():
  args = dict()
  
  for arg in sys.argv[1:]:
    eq_idx =  arg.find('=')
    if eq_idx == -1:
      args[arg] = True;
      continue;

    value = arg[eq_idx + 1:]
    
    if value == 'true':
      value = True
    
    if value == 'false':
      value = False
    
    args[arg[0: eq_idx]] = value

  return args


def main():
  for arg, value in minimize_args().items():
    print(arg, '-', value)
    os.environ[arg] = str(value)

  # test
  # for arg in minimize_args():
  #   os.system('echo ' + arg + ' is: ' + env_getter(arg));

if __name__ == '__main__':
  main()
